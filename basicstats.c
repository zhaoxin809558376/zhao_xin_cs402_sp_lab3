#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

// read file
int open_file(char *file_name)
{
    if(fopen(file_name,"r") == NULL)
        return -1;// if there is no data in file,return -1
    return 0;
}

// function to get mean
double get_mean(float *p,int n)
{
    double sum = 0;
    for (int i = 0; i < n;i++)
        sum = sum+p[i];
    double mean = sum/n;
    return mean;
}

// function to get stddev
double get_stddev(float *p,int n)
{
    double stddev = 0;
    double mean = get_mean(p, n);
    for (int i = 0; i < n;i++)
    {
        stddev += pow(p[i] - mean, 2);
    }
    return sqrt(stddev / n);
}

//function to get median
double get_median(float *float_array,int n)
{
    if(n%2 == 1)
        return (float_array[n / 2]);
    else
    {
        double min = float_array[n / 2 - 1];
        double max = float_array[n / 2];
        return ((min + max) / 2);
    }
}

// function to sort array
void swap(float *xp,float *yp)
{
    float temp = *xp;
    *xp = *yp;
    *yp = temp;
}
float sort(float *float_array,int n)
{
    int i, j, min_index;
    // one by one move boundary of unsorted subarray
    for (i = 0; i < n - 1;i++)
    {
        min_index = i;
        for (j = i + 1; j < n;j++)
        if(float_array[j]<float_array[min_index])
            min_index = j;
        // swap the found minimum element with the first element
        swap(&float_array[min_index], &float_array[i]);
    }
}



int main(int argc,char *argv[])
{
    if(argc<2)
    {
        printf("Pass filename to read from .. \n");
        return 0;
    }
    char *file_name = argv[1];
    int n = 20;
    int array_length = 0;
    float *float_array = (float *)malloc(n*sizeof(float));// create a float_array 

    // read file
    if(open_file(file_name)==-1)
    {
        printf("Error reading file.\n");
        return -1;
    }
    FILE *fp = fopen(file_name,"r");
    while(!feof(fp))
    {
        fscanf(fp, "%f\n", (float_array+array_length));
        array_length++;
        if(array_length==n)//The float array length is equal to max capacity
        {
            // create new array with twice size
            float *new_array = (float *)malloc(n * 2 * sizeof(float));
            // copy values from the old temp array to new array
            memcpy(new_array, float_array, array_length * sizeof(float));
            // free memory
            free(float_array);
            float_array = new_array;
            // increase the size of n
            n = n * 2;
        }
    }
    fclose(fp);

    // result
    printf("Results:\n");
    printf("-----------------------------\n");
    double mean = get_mean(float_array, array_length);
    double stddev = get_stddev(float_array, array_length);
    printf("The array length: %d\n", array_length);
    printf("The mean is: %.3f\n", mean);
    sort(float_array, array_length);
    double median = get_median(float_array, array_length);
    printf("The median is: %.3f\n", median);
    printf("The stddev is: %.3f\n", stddev);
    printf("Unused array cspacity: %d\n", n - array_length);
}
