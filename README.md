# README #

This readme document will tell the user step by step how to operate and introduce the code.

### How to compile and execute ###

* First, type the command in the terminal

	**gcc -o basicstats basicstats.c -g -lm**
	
* Second, type the command in the terminal

	**./basicstats small.txt**
	
	**./basicstats large.txt**
	

### Function ###

* int open_file(char *fname)
* double get_mean(float *p,int n)
* double get_stddev(float *p,int n)
* double get_median(float *float_array,int n)
* void swap(float *xp,float *yp)
* float sort(float *float_array,int n)

### Result ###

** ./basicstats small.txt**

		Results:

		The array length: 12
	
		The mean is: 85.776
	
		The median is: 67.470
	
		The stddev is: 90.380
	
		Unused array cspacity: 8

** ./basicstats large.txt**

		Results:

		The array length: 12

		The mean is: 85.776

		The median is: 67.470
	
		The stddev is: 90.380

		Unused array cspacity: 8

